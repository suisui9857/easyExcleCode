package com.suisui.entity;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 填充数据
 **/
public class FileDate {
    private String name;
    private Double number;

    public FileDate() {
    }

    @Override
    public String toString() {
        return "FileDate{" +
                "name='" + name + '\'' +
                ", number=" + number +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getNumber() {
        return number;
    }

    public void setNumber(Double number) {
        this.number = number;
    }

    public FileDate(String name, Double number) {
        this.name = name;
        this.number = number;
    }
}