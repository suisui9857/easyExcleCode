package com.suisui.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;

import java.util.Date;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: excel读到的数据
 **/
public class DemoData {
    //根据标题匹配
   // @ExcelProperty("日期标题")
    //通过下标获取
    @ExcelProperty(index = 1)
    //excel日期格式转换
    @DateTimeFormat("yyyy年MM月dd日 HH时mm分ss秒")
    private Date date;
    //@ExcelProperty("字符串标题")
    @ExcelProperty(index = 0)
    private String string;
    //@ExcelProperty("数字标题")
    @ExcelProperty(index = 2)
    //建议String类型@NumberFormat("#.##")
    private Double aDouble;

    public DemoData() {
    }

    public DemoData(String string, Date date, Double aDouble) {
        this.string = string;
        this.date = date;
        this.aDouble = aDouble;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }

    @Override
    public String toString() {
        return "DemoData{" +
                "string='" + string + '\'' +
                ", date=" + date +
                ", aDouble=" + aDouble +
                '}';
    }
}