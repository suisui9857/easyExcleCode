package com.suisui.test;

import com.sun.deploy.nativesandbox.comm.Response;

import java.net.URLEncoder;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 上传下载测试类
 **/
public class UploadTest {

    //下载测试类，需在web环境下
    public void test1(){
        //1.设置响应头
        //response.setContentType("application/vnd.ms-excel");
        //response.setCharacterEncoding("utf-8");
        //2.设置防止文件名中文乱码
        //String fileName = URLEncoder.encode("中文文件名","utf-8");
        //response.setHeader("Content-disposition","attachment:filename="+fileName+".xlsx");
        //3.构建写入到excle的数据
     //  //根据User模板构建数据
        //        ArrayList<User> list = new ArrayList<>();
        //        User user1 = new User(1001,"李磊","男",1000.200,new Date());
        //        User user2 = new User(1002,"韩梅梅","女",100200.00,new Date());
        //        User user3 = new User(1003,"汤姆","男",10000.0,new Date());
        //        User user4 = new User(1004,"猫","女",10000.0,new Date());
        //        User user5 = new User(1005,"狗","男",10000.0,new Date());
        //        list.add(user1);
        //        list.add(user2);
        //        list.add(user3);
        //        list.add(user4);
        //        list.add(user5);
        //通过输出流写入
        //EasyExcel.write(response.getOutputStream().User.class).sheet("用户信息").doWrite(users);
    }

    /**
     * 前端文件上传excel之后解析
     */
    public void test2() {
        //DiskFileItemFactory factory = new DiskFileItemFactory();
        //ServletFileUpload fileUpload =  new ServletFileUpload(factory);
        //设置单个文件为3M
        // fileUpload.setFileSizeMax(1024*1024*3);
        //总文件大小为30M
        // fileUpload.setSizeMax(1024*1024*30);
        //fileUpload.
        //解析req对象，生成表单项
        //List<FileItem> list = fileUpload.parseRequest(req);
        //遍历，解析
        //for(FileItem fileItem : list){
        //判断是否为附件
        //if(!fileItem.isFormField()){
        // 附件
        //获取输入流
        //InputStream inputStream =  fileItem.getInputStream();
            //  EasyExcel.read(inputStream, User.class, new AnalysisEventListener<User>(){
        //            /**
        //             * 每解析一行excel数据，就会被调用一次
        //             * @param demoData
        //             * @param analysisContext
        //             */
        //            @Override
        //            public void invoke(User user, AnalysisContext analysisContext) {
        //                System.out.println("解析数据为：" + user.toString());
        //                //调用数据库将解析到的数据保存到数据库里
        //            }
        //
        //            /**
        //             * 全部解析完被调用
        //             * @param analysisContext
        //             */
        //            @Override
        //            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        //                System.out.println("全部解析完成");
        //            }
        //        }).sheet().doRead();
      // }else{
        //普通

        // }

    }
}