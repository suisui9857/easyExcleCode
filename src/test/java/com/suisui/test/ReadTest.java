package com.suisui.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.suisui.entity.DemoData;
import org.junit.Test;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 读操作
 **/
public class ReadTest {

    /**
     * 默认读取方式---excel数据表头和对象的属性字段顺序一致
     * * 通过名称读取： 实体类加@ExcelProperty("日期标题")
     * * 根据下标读取：实体类加@ExcelProperty(index = 1)
     */
    @Test
    public void test1(){
        //读取文件路径
        String fileName = "user10.xlsx";
        //读取excel
        EasyExcel.read(fileName, DemoData.class, new AnalysisEventListener<DemoData>(){
            /**
             * 每解析一行excel数据，就会被调用一次
             * @param demoData
             * @param analysisContext
             */
            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println("解析数据为：" + demoData.toString());
                //调用数据库将解析到的数据保存到数据库里
            }

            /**
             * 全部解析完被调用
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("全部解析完成");
            }
        }).sheet().doRead();
        System.out.println("=======================================");


        //读取excel--方式2
        ExcelReader build = EasyExcel.read(fileName, DemoData.class, new AnalysisEventListener<DemoData>() {
            /**
             * 每解析一行excel数据，就会被调用一次
             * @param demoData
             * @param analysisContext
             */
            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println("解析数据为：" + demoData.toString());
                //调用数据库将解析到的数据保存到数据库里
            }

            /**
             * 全部解析完被调用
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("全部解析完成");
            }
        }).build();
        //创建sheet对象，读取excel的第一个sheet(下级从0开始)
        ReadSheet sheet = EasyExcel.readSheet(0).build();
        build.read(sheet);
        //关闭流操作
        build.finish();
    }

    /**
     * 读一个excel的多个sheet
     */
    @Test
    public void test2() {
        //读取文件路径
        String fileName = "user10.xlsx";
        //读取excel
        EasyExcel.read(fileName, DemoData.class, new AnalysisEventListener<DemoData>() {
            /**
             * 每解析一行excel数据，就会被调用一次
             *
             * @param demoData
             * @param analysisContext
             */
            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println("解析数据为：" + demoData.toString());
                //调用数据库将解析到的数据保存到数据库里
            }

            /**
             * 全部解析完被调用
             *
             * @param analysisContext
             */
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("全部解析完成");
            }
        }).doReadAll();//----保证所以sheet都使用DemoData.class模板
    }



    /**
     * 读一个excel的某几个sheet
     */
    @Test
    public void test3() {
        //读取文件路径
        String fileName = "user10.xlsx";
        //构建excelReader对象
        ExcelReader excelReader = EasyExcel.read(fileName).build();
        //构建sheet标签对象
        ReadSheet sheet1 = EasyExcel.readSheet(0).head(DemoData.class).registerReadListener(new AnalysisEventListener<DemoData>() {

            //每解析一行被调用
            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println("sheet1解析数据：" + demoData);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {

            }
        }).build();
        ReadSheet sheet2 = EasyExcel.readSheet(1).head(DemoData.class).registerReadListener(new AnalysisEventListener<DemoData>() {

            //每解析一行被调用
            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                System.out.println("sheet2解析数据：" + demoData);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {

            }
        }).build();
        excelReader.read(sheet1,sheet2);
        excelReader.finish();
    }
}