package com.suisui.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.alibaba.excel.write.metadata.WriteSheet;
import com.suisui.pojo.*;
import org.junit.Test;

import javax.jws.soap.SOAPBinding;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

/**
 * @author wmh
 * @version 1.0 2022/5/23
 * @Description: 写入测试
 **/
public class WriteTest {
    @Test
    public void test1(){
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user1.xlsx";
        //根据User模板构建数据
        ArrayList<User> list = new ArrayList<>();
        User user1 = new User(1001,"李磊","男",10000.0,new Date());
        User user2 = new User(1002,"韩梅梅","女",10000.0,new Date());
        User user3 = new User(1003,"汤姆","男",10000.0,new Date());
        User user4 = new User(1004,"猫","女",10000.0,new Date());
        User user5 = new User(1005,"狗","男",10000.0,new Date());
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);
        //向excel中写数据，文件名，类型，标签名
        EasyExcel.write(fileName,User.class).sheet("用户信息").doWrite(list);
    }

    @Test
    public void test2(){
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user2.xlsx";
        //根据User模板构建数据
        ArrayList<User> list = new ArrayList<>();
        User user1 = new User(1001,"李磊","男",10000.0,new Date());
        User user2 = new User(1002,"韩梅梅","女",10000.0,new Date());
        User user3 = new User(1003,"汤姆","男",10000.0,new Date());
        User user4 = new User(1004,"猫","女",10000.0,new Date());
        User user5 = new User(1005,"狗","男",10000.0,new Date());
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);
        //构建ExcelWriter对象
        ExcelWriter excelWriter = EasyExcel.write(fileName, User.class).build();
        //创建sheet对象
        WriteSheet writerSheet = EasyExcel.writerSheet("用户信息").build();
        //信息写入
        excelWriter.write(list,writerSheet);
        //关闭
        excelWriter.finish();
    }

    /**
     * 排除模板中的列
     */
    @Test
    public void test3(){
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user3.xlsx";
        //根据User模板构建数据
        ArrayList<User> list = new ArrayList<>();
        User user1 = new User(1001,"李磊","男",10000.0,new Date());
        User user2 = new User(1002,"韩梅梅","女",10000.0,new Date());
        User user3 = new User(1003,"汤姆","男",10000.0,new Date());
        User user4 = new User(1004,"猫","女",10000.0,new Date());
        User user5 = new User(1005,"狗","男",10000.0,new Date());
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        //设置排除的属性
        HashSet<String> set = new HashSet<>();
        set.add("hireDate");
        set.add("salary");
        //写入
        EasyExcel.write(fileName, User.class).excludeColumnFiledNames(set).sheet("用户信息").doWrite(list);
    }



    /**
     * 按照顺序排列
     */
    @Test
    public void test4(){
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user4.xlsx";
        //根据User模板构建数据
        ArrayList<User> list = new ArrayList<>();
        User user1 = new User(1001,"李磊","男",10000.0,new Date());
        User user2 = new User(1002,"韩梅梅","女",10000.0,new Date());
        User user3 = new User(1003,"汤姆","男",10000.0,new Date());
        User user4 = new User(1004,"猫","女",10000.0,new Date());
        User user5 = new User(1005,"狗","男",10000.0,new Date());
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        //设置排除的属性
        HashSet<String> set = new HashSet<>();
        set.add("userId");
        set.add("userName");
        set.add("hireDate");
        set.add("salary");
        //写入
        EasyExcel.write(fileName, User.class).includeColumnFiledNames(set).sheet("用户信息").doWrite(list);
    }


    /**
     * 复杂头
     */
    @Test
    public void test5(){
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user5.xlsx";
        //根据User模板构建数据
        ArrayList<ComplexHeadUser> list = new ArrayList<>();
        ComplexHeadUser user1 = new ComplexHeadUser(1111,"李磊1","男");
        ComplexHeadUser user2 = new ComplexHeadUser(1112,"李磊2","男");
        ComplexHeadUser user3 = new ComplexHeadUser(1113,"李磊3","男");
        ComplexHeadUser user4 = new ComplexHeadUser(1114,"李磊4","男");
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        //设置排除的属性
        EasyExcel.write(fileName, ComplexHeadUser.class).sheet("用户信息").doWrite(list);
    }


    /**
     * 重复写到同一个excel的sheet标签中
     */
    @Test
    public void test6() {
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user6.xlsx";
        //根据User模板构建数据
        ArrayList<User> list = new ArrayList<>();
        User user1 = new User(1001, "李磊", "男", 10000.0, new Date());
        User user2 = new User(1002, "韩梅梅", "女", 10000.0, new Date());
        User user3 = new User(1003, "汤姆", "男", 10000.0, new Date());
        User user4 = new User(1004, "猫", "女", 10000.0, new Date());
        User user5 = new User(1005, "狗", "男", 10000.0, new Date());
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        //构建ExcelWriter对象
        ExcelWriter excelWriter = EasyExcel.write(fileName, User.class).build();
        //创建sheet对象
        WriteSheet writerSheet = EasyExcel.writerSheet("用户信息").build();
        //信息重复写入
        for (int i = 0; i < 5; i++) {
            excelWriter.write(list, writerSheet);
        }
        //关闭
        excelWriter.finish();
    }


        /**
         * 重复写到同一个excel的不同sheet标签中
         */
        @Test
        public void test7(){
            //创建一个excel文档，默认当前项目的目录下，文件自己创建
            String fileName = "user7.xlsx";
            //根据User模板构建数据
            ArrayList<User> list = new ArrayList<>();
            User user1 = new User(1001,"李磊","男",10000.0,new Date());
            User user2 = new User(1002,"韩梅梅","女",10000.0,new Date());
            User user3 = new User(1003,"汤姆","男",10000.0,new Date());
            User user4 = new User(1004,"猫","女",10000.0,new Date());
            User user5 = new User(1005,"狗","男",10000.0,new Date());
            list.add(user1);
            list.add(user2);
            list.add(user3);
            list.add(user4);
            list.add(user5);

            //构建ExcelWriter对象
            ExcelWriter excelWriter = EasyExcel.write(fileName, User.class).build();
            for (int i = 0; i < 5; i++) {
                //创建sheet对象
                WriteSheet writerSheet = EasyExcel.writerSheet("用户列表"+i).build();
                //写数据
                excelWriter.write(list,writerSheet);
            }
            //关闭
            excelWriter.finish();
        }


    /**
     * 日期数字格式化
     */
    @Test
    public void test8(){
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user8.xlsx";
        //根据User模板构建数据
        ArrayList<User> list = new ArrayList<>();
        User user1 = new User(1001,"李磊","男",1000.200,new Date());
        User user2 = new User(1002,"韩梅梅","女",100200.00,new Date());
        User user3 = new User(1003,"汤姆","男",10000.0,new Date());
        User user4 = new User(1004,"猫","女",10000.0,new Date());
        User user5 = new User(1005,"狗","男",10000.0,new Date());
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        //向excel中写数据，文件名，类型，标签名
        EasyExcel.write(fileName,User.class).sheet("用户信息").doWrite(list);
    }


    /**
     * 图片存入
     */
    @Test
    public void test9() throws Exception {
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user9.xlsx";
        ArrayList<ImageDate> list = new ArrayList<>();
        //根据模板构建数据
        ImageDate imageDate = new ImageDate();
        imageDate.setFile(new File("fm.jpg"));
        imageDate.setInputStream(new FileInputStream(new File("fm.jpg")));
        imageDate.setString("fm.jpg");
        //创建一个指定大小的二进制数组
        byte[] bytes = new byte[(int) new File("fm.jpg").length()];
        FileInputStream stream = new FileInputStream("fm.jpg");
        //将文件流写入到二进制数组中
        stream.read(bytes,0,(int) new File("fm.jpg").length());
        imageDate.setByteArrays(bytes);

        imageDate.setUrl(new URL("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.jj20.com%2Fup%2Fallimg%2F1114%2F113020142315%2F201130142315-1-1200.jpg&refer=http%3A%2F%2Fimg.jj20.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1655946837&t=83859771f6a4e369535370da854e1b86"));
        //向excel中写数据，文件名，类型，标签名
        list.add(imageDate);
        EasyExcel.write(fileName,ImageDate.class).sheet("图片信息").doWrite(list);
    }



    /**
     * 设置表格行高宽高
     */
    @Test
    public void test10() throws Exception {
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user10.xlsx";
        //根据模板构建数据
        ArrayList<WidthAndHeightData> list = new ArrayList<>();
        WidthAndHeightData widthAndHeightData = new WidthAndHeightData("123",new Date(),255.0);
        list.add(widthAndHeightData);
        EasyExcel.write(fileName,WidthAndHeightData.class).sheet("数据信息").doWrite(list);

    }


    /**
     * 设置表格样式
     */
    @Test
    public void test11() {
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user11.xlsx";
        //根据模板构建数据
        ArrayList<StyleDate> list = new ArrayList<>();
        StyleDate styleDate = new StyleDate("123", new Date(), 255.0);
        StyleDate styleDate1 = new StyleDate("123", new Date(), 255.0);
        list.add(styleDate);
        list.add(styleDate1);
        EasyExcel.write(fileName,StyleDate.class).sheet("数据信息").doWrite(list);

    }



    /**
     * 合并单元格
     */
    @Test
    public void test12() {
        //创建一个excel文档，默认当前项目的目录下，文件自己创建
        String fileName = "user12.xlsx";
        //根据模板构建数据
        ArrayList<MergeData> list = new ArrayList<>();
        MergeData styleDate = new MergeData("123", new Date(), 255.0);
        MergeData styleDate1 = new MergeData("123", new Date(), 255.0);
        list.add(styleDate);
        list.add(styleDate1);
        EasyExcel.write(fileName,MergeData.class).sheet("数据信息").doWrite(list);

    }
}