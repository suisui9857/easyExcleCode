package com.suisui.test;

import com.alibaba.excel.EasyExcel;
import com.suisui.entity.FileDate;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;

import java.util.ArrayList;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 根据模板填充
 **/
public class FullTest {
    @Test
    public void test1(){
        //根据哪个模板填充
        String fileName = "模板.xls";
        //填充之后的excel
        String fullFileName = "fullFile.xls";
        //构建数据
        FileDate fileDate1 = new FileDate("小米1",22.0);
        FileDate fileDate2 = new FileDate("小米2",22.0);
        FileDate fileDate3 = new FileDate("小米3",22.0);
        FileDate fileDate4 = new FileDate("小米4",22.0);
        //进行填充
        EasyExcel.write(fullFileName).withTemplate(fileName).sheet().doFill(fileDate1);
    }

    @Test
    public void test2(){
        //根据哪个模板填充
        String fileName = "模板2.xls";
        //填充之后的excel
        String fullFileName = "fullFile2.xls";
        //构建数据
        ArrayList<FileDate> dates = new ArrayList<>();
        FileDate fileDate1 = new FileDate("小米1",22.0);
        FileDate fileDate2 = new FileDate("小米2",22.0);
        FileDate fileDate3 = new FileDate("小米3",22.0);
        FileDate fileDate4 = new FileDate("小米4",22.0);
        dates.add(fileDate1);
        dates.add(fileDate2);
        dates.add(fileDate3);
        dates.add(fileDate4);
        //进行填充
        EasyExcel.write(fullFileName).withTemplate(fileName).sheet().doFill(dates);
    }
 }