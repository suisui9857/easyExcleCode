package com.suisui.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.*;
import org.apache.poi.ss.usermodel.FillPatternType;

import java.util.Date;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 属性设置模板
 **/
//头背景设置成红色
@HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND,fillForegroundColor = 10)
//头字体设置成20
@HeadFontStyle(fontHeightInPoints = 10)
//字符串内容的背景设置为绿色
@ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND,fillForegroundColor = 17)
//字符串的内容字体设置成20
@ContentFontStyle(fontHeightInPoints = 20)
public class StyleDate {

    //字符串的头背景设置成粉红色
    @HeadStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND,fillForegroundColor = 14)
    //字符串的头字体设置成20
    @HeadFontStyle(fontHeightInPoints = 20)
    //字符串内容的背景设置为天蓝
    @ContentStyle(fillPatternType = FillPatternType.SOLID_FOREGROUND,fillForegroundColor = 40)
    //字符串的内容字体设置成30
    @ContentFontStyle(fontHeightInPoints = 30)
    @ExcelProperty("字符串标题")
    private String string;
    @ExcelProperty("日期标题")
    private Date date;
    @ExcelProperty("数字标题")
    private Double aDouble;

    public StyleDate(String string, Date date, Double aDouble) {
        this.string = string;
        this.date = date;
        this.aDouble = aDouble;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }
}