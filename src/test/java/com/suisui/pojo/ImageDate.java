package com.suisui.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.converters.string.StringImageConverter;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 图片数据格式
 **/
public class ImageDate {
    //使用抽象文件表示一个图片
    private File file;

    //使用输入流保存一个图片
    private InputStream inputStream;

    //使用String类型保存图片，需使用StringImageConverter转换器
    @ExcelProperty(converter = StringImageConverter.class)
    private String string;

    //使用二进制保存一个图片
    private byte[] byteArrays;

    @Override
    public String toString() {
        return "ImageDate{" +
                "file=" + file +
                ", inputStream=" + inputStream +
                ", string='" + string + '\'' +
                ", byteArrays=" + Arrays.toString(byteArrays) +
                ", url=" + url +
                '}';
    }

    public ImageDate() {
    }

    public ImageDate(File file, InputStream inputStream, String string, byte[] byteArrays, URL url) {
        this.file = file;
        this.inputStream = inputStream;
        this.string = string;
        this.byteArrays = byteArrays;
        this.url = url;
    }

    //使用URL保存一个图片
    private URL url;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public byte[] getByteArrays() {
        return byteArrays;
    }

    public void setByteArrays(byte[] byteArrays) {
        this.byteArrays = byteArrays;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
}