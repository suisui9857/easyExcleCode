package com.suisui.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentLoopMerge;
import com.alibaba.excel.annotation.write.style.OnceAbsoluteMerge;

import java.util.Date;

/**
 * @author wmh
 * @version 1.0 2022/5/24
 * @Description: 合并单元格
 **/

/**
 * firstRowIndex：起始行索引
 * lastRowIndex：结束行索引
 * firstColumnIndex：起始列索引
 * lastColumnIndex：结束列索引
 */
@OnceAbsoluteMerge(firstRowIndex = 1,lastRowIndex = 2,firstColumnIndex = 1,lastColumnIndex = 2)
public class MergeData {

    //每隔两行合并一次(竖着合并单元格)
    @ContentLoopMerge(eachRow = 2)
    @ExcelProperty("字符串标题")
    private String string;
    @ExcelProperty("日期标题")
    private Date date;
    @ExcelProperty("数字标题")
    private Double aDouble;

    public MergeData(String string, Date date, Double aDouble) {
        this.string = string;
        this.date = date;
        this.aDouble = aDouble;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getaDouble() {
        return aDouble;
    }

    public void setaDouble(Double aDouble) {
        this.aDouble = aDouble;
    }
}