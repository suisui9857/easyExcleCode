package com.suisui.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.format.NumberFormat;

import java.util.Date;

/**
 * @author wmh
 * @version 1.0 2022/5/23
 * @Description: 用户
 * 创建user类模板，通过User类模板，想Excel表格中写数据
 * @ExcelProperty("用户编号")  描述excel的表格头
 **/
public class User {

    @ExcelProperty(value = "用户编号",index = 0)
    private Integer userId;
    @ExcelProperty(value = "用户名称",index = 1)
    private String userName;
    @ExcelProperty(value = "用户性别",index = 3)
    private String gender;
    @ExcelProperty(value = "用户薪资",index = 2)
    @NumberFormat("#.##")
    private Double salary;
    @DateTimeFormat("yyyy年MM月dd日 HH:mm:ss")
    @ExcelProperty(value = "入职时间",index = 4)
    private Date hireDate;

    public User() {
    }

    public User(Integer userId, String userName, String gender, Double salary, Date hireDate) {
        this.userId = userId;
        this.userName = userName;
        this.gender = gender;
        this.salary = salary;
        this.hireDate = hireDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }
}