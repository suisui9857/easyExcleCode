package com.suisui.pojo;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @author wmh
 * @version 1.0 2022/5/23
 * @Description: 复杂头
 **/
public class ComplexHeadUser {
    @ExcelProperty({"用户主题1","用户编号"})
    private Integer userId;
    @ExcelProperty({"用户主题1","用户名"})
    private String userName;
    @ExcelProperty({"用户主题1","用户性别"})
    private String gender;

    @Override
    public String toString() {
        return "ComplexHeadUser{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }

    public ComplexHeadUser() {
    }

    public ComplexHeadUser(Integer userId, String userName, String gender) {
        this.userId = userId;
        this.userName = userName;
        this.gender = gender;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}